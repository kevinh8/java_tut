import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CalculatorController {
   private CalculatorView theView;
   private CalculatorModel theModel;

   public CalculatorController(CalculatorView theView, CalculatorModel theModel) {
      this.theView = theView;
      this.theModel = theModel;
   
      this.theView.addCalculationListener(new CalculateListener());
   }
   
   class CalculateListener implements ActionListener {
      public void actionPerformed(ActionEvent arg0) {
         int firstNum, secondNum = 0;
   
         try {
            firstNum = theView.getFirstNum();
            secondNum = theView.getSecondNum();
         
            theModel.addTwoNumbers(firstNum, secondNum);
            theView.setCalcSolution(theModel.getCalculationValue());   
         }
         catch(NumberFormatException ex) {
            theView.displayErrorMessage("Enter 2 integers");
         }
      }
   }
}
