package com.kevin.springapp.Controller;

import java.util.Collection;

import com.kevin.springapp.Service.StudentService;
import com.kevin.springapp.Entity.Student;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/students")
public class StudentController {

    @Autowired //Dependency injection rather than using "= new StudentService ..." each time
    private StudentService studentService;

    @RequestMapping(method=RequestMethod.GET)
    public void getAllStudents() {
        this.studentService.getAllStudents();
    }

/*    
    @RequestMapping(method=RequestMethod.GET)
    public Collection<Student> getAllStudents() {
        return this.studentService.getAllStudents();
    }

    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public Student getStudentById(@PathVariable("id") int id) {
        return this.studentService.getStudentById(id);
    }

    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    public void removeStudentById(@PathVariable("id") int id) {
        this.studentService.removeStudentById(id);
    }
    
    @RequestMapping(method=RequestMethod.PUT, consumes=MediaType.APPLICATION_JSON_VALUE)
    public void updateStudent(@RequestBody Student student) {
        this.studentService.updateStudent(student);
    }

    @RequestMapping(method=RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
    public void insertStudent(@RequestBody Student student) {
        this.studentService.insertStudent(student);
    }
*/
}