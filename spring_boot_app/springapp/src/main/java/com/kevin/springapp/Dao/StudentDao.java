package com.kevin.springapp.Dao;

import com.kevin.springapp.Entity.Student;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentDao extends MongoRepository<Student, String> {
}