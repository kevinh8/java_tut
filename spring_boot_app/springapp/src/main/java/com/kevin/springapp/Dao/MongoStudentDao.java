package com.kevin.springapp.Dao;

import com.kevin.springapp.Entity.Student;
import com.kevin.springapp.Dao.StudentDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

@Repository
@Qualifier("mongoData")
public class MongoStudentDao {
  
    @Autowired
    private StudentDao mongoDao;

    public void getAllStudents() {
        System.out.print("Accessed ... \n");
        Student s1 = new Student("Wally", "Engineering");
        Student s2 = new Student("Panda", "Commerce");
        Student s3 = new Student("Yuri", "Music");
        
        System.out.print("Students created ... \n");
        this.mongoDao.save(s1);
        this.mongoDao.save(s2);
        this.mongoDao.save(s3);
        System.out.print("Students entered into database ... \n\n");
        
        int i = 0;
        for(Student s : mongoDao.findAll()) {
            System.out.println(s.getName());
            i ++; 
        }
        System.out.println("Total entries: " + i);
        
    }
}