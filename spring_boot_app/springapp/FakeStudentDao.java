package com.kevin.springapp.Dao;

import java.util.HashMap;
import java.util.Map;
import java.util.Collection;

import com.kevin.springapp.Entity.Student;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

@Repository
@Qualifier("fakeData")
public class FakeStudentDao /*implements StudentDao */{

    private static Map<Integer, Student> students;

    static {
        students = new HashMap<Integer, Student>() {
            {
                put(1, new Student(1, "Kevin", "Computer Science"));
                put(69, new Student(2, "Katie", "Physiotherapy"));
                put(3, new Student(3, "John", "Mathematics"));
            }
        };
    }

    @Override
    public Collection<Student> getAllStudents() {
        return this.students.values();
    }

    @Override
    public Student getStudentById(int id) {
        return this.students.get(id);
    }

    @Override
    public void removeStudentById(int id) {
        this.students.remove(id);
    }

    @Override
    public void updateStudent(Student student) {
        Student s = this.students.get(student.getId());
        s.setName(student.getName());
        s.setCourse(student.getCourse());
        this.students.put(student.getId(), s);
    }

    @Override
    public void insertStudent(Student student) {
        this.students.put(student.getId(), student);
    }
}