import java.util.Scanner;
import java.util.HashMap;

public class HashData {

   public static HashMap<Integer, String> buildMap(Student[] students) {
      HashMap<Integer, String> map = new HashMap<Integer, String>();
      for(Student s : students) {
         map.put(s.number, s.name); 
      } 
      return map;
   }

   public static void main(String agrc[]) {
      Scanner input = new Scanner(System.in);
      int i;
      Student[] studentStore = new Student[5];

      for(i = 0; i < 5; i++) {
         System.out.print("Enter student ID: ");
         int id = Integer.parseInt(input.next());
         System.out.print("Enter student name: ");
         String name = input.next(); 
         
         studentStore[i] = new Student(id, name);
      }
     
      HashMap<Integer, String> hmap = buildMap(studentStore);
   }

}


