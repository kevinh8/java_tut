import java.util.Scanner;
import java.util.LinkedList;

public class HashTable {
   public static void main(String argc[]) {
      int i;
      @SuppressWarnings("unchecked")
      LinkedList<String>[] hashTable = new LinkedList[5];
      Scanner input = new Scanner(System.in);

      for (i = 0; i < hashTable.length; i++) {
         hashTable[i] = new LinkedList<String>();
      }

      while (true) {
         System.out.print("Enter a string: ");
         String val = input.next();
         if (val.equals("done")) {
            break;
         }
         int index = Math.abs(val.hashCode() % hashTable.length);
         hashTable[index].add(val);
         System.out.print("Value is: '" + val + "'"); 
         System.out.print(" HashCode: " + val.hashCode());
         System.out.print(" Index: " + index);
         System.out.print("\n");
      }

      for (i = 0; i < hashTable.length; i++) {
         System.out.println("\n\n\nList " + i + ": ");
         System.out.print(hashTable[i]);
      }
   }
}
